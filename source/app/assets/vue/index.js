import Vue from "vue";
import App from "~/App";
import router from "~/router";
import store from "~/components/submodules/comment/store";
import { BootstrapVue } from 'bootstrap-vue'

// Import Bootstrap an BootstrapVue CSS files (order is important)
import 'bootstrap/dist/css/bootstrap.css'
import 'bootstrap-vue/dist/bootstrap-vue.css'

// // Make BootstrapVue available throughout your project
Vue.use(BootstrapVue)

new Vue({
  components: { App },
  template: "<App/>",
  render: h => h(App),
  store,
  router
}).$mount("#app");
