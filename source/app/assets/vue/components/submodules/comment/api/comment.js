import axios from "axios";

export default {
  create(message) {
    return axios.post("/api/comments", {
      message: message
    });
  },
  findAll() {
    return axios.get("/api/comments");
  }
};