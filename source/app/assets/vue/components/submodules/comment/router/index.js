import Comments from '~/components/submodules/comment/Main'

var routes = [{
  path: '/comments',
  name: 'Comments',
  component: Comments,
  meta: { requiresAuth: true }
}]

export default {
  routes
}
