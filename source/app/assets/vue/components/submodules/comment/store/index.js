import Vue from "vue";
import Vuex from "vuex";
import SecurityModule from "~/store/security";
import CommentModule from "./comments";

Vue.use(Vuex);

export default new Vuex.Store({
  modules: {
    comment: CommentModule,
    security: SecurityModule,
  }
});