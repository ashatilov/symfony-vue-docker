import CommentAPI from "../api/comment";

const CREATING_COMMENT = "CREATING_COMMENT",
  CREATING_COMMENT_SUCCESS = "CREATING_COMMENT_SUCCESS",
  CREATING_COMMENT_ERROR = "CREATING_COMMENT_ERROR",
  FETCHING_COMMENTS = "FETCHING_COMMENTS",
  FETCHING_COMMENTS_SUCCESS = "FETCHING_COMMENTS_SUCCESS",
  FETCHING_COMMENTS_ERROR = "FETCHING_COMMENTS_ERROR";

export default {
  namespaced: true,
  state: {
    isLoading: false,
    error: null,
    comments: []
  },
  getters: {
    isLoading(state) {
      return state.isLoading;
    },
    hasError(state) {
      return state.error !== null;
    },
    error(state) {
      return state.error;
    },
    hasComments(state) {
      return state.comments.length > 0;
    },
    Comments(state) {
      return state.comments;
    },
    canCreatePost() {
      return this.$store.getters["security/hasRole"]("ROLE_USER");
    }
  },
  mutations: {
    [CREATING_COMMENT](state) {
      state.isLoading = true;
      state.error = null;
    },
    [CREATING_COMMENT_SUCCESS](state, post) {
      state.isLoading = false;
      state.error = null;
      state.comments.unshift(post);
    },
    [CREATING_COMMENT_ERROR](state, error) {
      state.isLoading = false;
      state.error = error;
      state.comments = [];
    },
    [FETCHING_COMMENTS](state) {
      state.isLoading = true;
      state.error = null;
      state.comments = [];
    },
    [FETCHING_COMMENTS_SUCCESS](state, comments) {
      state.isLoading = false;
      state.error = null;
      state.comments = comments;
    },
    [FETCHING_COMMENTS_ERROR](state, error) {
      state.isLoading = false;
      state.error = error;
      state.comments = [];
    }
  },
  actions: {
    async create({ commit }, message) {
      commit(CREATING_COMMENT);
      try {
        let response = await CommentAPI.create(message);
        commit(CREATING_COMMENT_SUCCESS, response.data);
        return response.data;
      } catch (error) {
        commit(CREATING_COMMENT_ERROR, error);
        return null;
      }
    },
    async findAll({ commit }) {
      commit(FETCHING_COMMENTS);
      try {
        let response = await CommentAPI.findAll();
        commit(FETCHING_COMMENTS_SUCCESS, response.data);
        return response.data;
      } catch (error) {
        commit(FETCHING_COMMENTS_ERROR, error);
        return null;
      }
    }
  }
};