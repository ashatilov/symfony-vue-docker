import Vue from "vue";
import Router from "vue-router";
import store from "~/store/security"
import Home from '~/components/submodules/home/router/index';
import Comment from '~/components/submodules/comment/router/index';
import Forbidden from '~/components/Forbidden';
import Error500 from '~/components/Error500';
import Error404 from '~/components/Error404';
import Login from '~/components/Login';

Vue.use(Router);

var routes = [
  {
    path: '/forbidden',
    name: 'Forbidden',
    component: Forbidden
  },
  {
    path: '/login',
    name: 'Login',
    component: Login
  },
  {
    path: '/error500',
    name: 'Error500',
    component: Error500
  },
  {
    path: '*',
    name: 'Error404',
    component: Error404
  }
];

routes = routes.concat(Comment.routes);
routes = routes.concat(Home.routes);

let router = new Router({
  mode: "history",
  routes: routes
});

router.beforeEach((to, from, next) => {

  if (to.matched.some(record => record.meta.requiresAuth)) {
    // this route requires auth, check if logged in
    // if not, redirect to login page.
    if (store.getters.isAuthenticated) {
      next();
    } else {
      next({
        path: "/login",
        query: { redirect: to.fullPath }
      });
    }
  } else {
    next(); // make sure to always call next()!
  }
});

export default router;