<?php
declare(strict_types=1);


namespace App\Model\User\Entity;


use Webmozart\Assert\Assert;

class Role
{
    const ROLE_ADMIN = 'admin';
    const ROLE_USER = 'user';
    private string $name;

    public function __construct(string $name)
    {
        Assert::oneOf($name, [
            self::ROLE_ADMIN,
            self::ROLE_USER,
        ]);
        $this->name = $name;
    }

    public function isEqual(Role $role)
    {
        return $this->getName() ===$role->getName();
    }

    public function getName(): string
    {
        return $this->name;
    }

}