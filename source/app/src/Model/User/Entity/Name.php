<?php

declare(strict_types=1);

namespace App\Model\User\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Класс обертка для Имени пользователя\
 * @ORM\Embeddable
 */
class Name
{
    /**
     * @ORM\Column(name="Имя", type="string", columnDefinition="COMMENT 'Имя пользователя'")
     */
    private string $firstName;

    public function __construct(string $firstName)
    {
        $this->firstName = $firstName;
    }

    public function getFirstName(): string
    {
        return $this->firstName;
    }

    public function setFirstName(string $firstName): void
    {
        $this->firstName = $firstName;
    }

}