<?php

declare(strict_types=1);

namespace App\Model\User\Entity;


use Doctrine\ORM\Mapping as ORM;

/**
 * Пользователь системы.
 *
 * @ORM\Entity
 * @ORM\HasLifecycleCallbacks
 * @ORM\Table(name="user_users", uniqueConstraints={
 *     @ORM\UniqueConstraint(columns={"email"})
 * })
 *
 */
class User
{
    /**
     * @ORM\Column(type="UuidType")
     * @ORM\Id
     */
    private Id $id;
    /**
     * @ORM\Column(type="user_user_email")
     */
    private Email $email;
    /**
     * @ORM\Embedded(class="Name")
     */
    private Name $name;

    /**
     * @ORM\Column(type="RoleType")
     */
    private $role;

    /**
     * @ORM\Column(type="string", name="password_hash", nullable=true)
     */
    private $passwordHash;

    public function __construct(Id $id, Email $email, Name $name)
    {

        $this->id = $id;
        $this->email = $email;
        $this->name = $name;
        $this->role = new Role(Role::ROLE_USER);
    }

    public function create(Id $id, Name $name, string $hash)
    {
        $user = new self($id, $name);
        $user->passwordHash = $hash;
        return $user;
    }

    public function getId(): Id
    {
        return $this->id;
    }

    public function getName(): Name
    {
        return $this->name;
    }
    public function getRole(): Role
    {
        return $this->role;
    }

}