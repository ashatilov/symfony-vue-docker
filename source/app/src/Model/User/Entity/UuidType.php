<?php

declare(strict_types=1);

namespace App\Model\User\Entity;


use Doctrine\DBAL\Platforms\AbstractPlatform;
use Doctrine\DBAL\Types\GuidType;

/**
 * Класс для работы с Uuid в качестве идентификатора
 */
class UuidType extends GuidType
{
    public const NAME = 'user_user_uuid';

    public function getSQLDeclaration($val, AbstractPlatform $platform)
    {
        return $val instanceof Id ? $val->getValue() : $val;
    }

    public function convertToPHPValue($value, AbstractPlatform $platform)
    {
        return !empty($value) ? new Id($value) : null;
    }

    public function getName()
    {
        return self::getName();
    }

    public function requiresSQLCommentHint(AbstractPlatform $platform)
    {
        return true;
    }
}