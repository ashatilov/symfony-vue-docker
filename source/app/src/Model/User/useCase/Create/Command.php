<?php
declare(strict_types=1);


namespace App\Model\User\useCase\Create;


use App\Model\User\Entity\Email;

class Command
{
    private string $email;
    private string $firstName;

    public function __construct(string $email, string $firstName)
    {
        $this->email = $email;
        $this->firstName = $firstName;
    }

    /**
     * @return Email
     */
    public function getEmail(): string
    {
        return $this->email;
    }

    /**
     * @return string
     */
    public function getFirstName(): string
    {
        return $this->firstName;
    }

}