<?php
namespace Deployer;

require 'recipe/symfony.php';
require 'recipe/yarn.php';

// Project name
set('application', 'my_project');

// Project repository
set('repository', 'git@bitbucket.org:ashatilov/symfony-vue-docker.git');

// [Optional] Allocate tty for git clone. Default value is false.
set('git_tty', true);

set('bin/console', '{{release_path}}/source/app/bin/console');

// Shared files/dirs between deploys
add('shared_files', [
   // 'source/app/.env.local.php',
]);
add('shared_dirs', []);

// Writable dirs by web server
add('writable_dirs', []);

//set('composer_options', '{{composer_action}} --verbose --prefer-dist --no-progress --no-interaction --no-dev --optimize-autoloader');
// Hosts

task('deploy:vendors', function () {
    if (!commandExist('unzip')) {
        writeln('<comment>To speed up composer installation setup "unzip" command with PHP zip extension https://goo.gl/sxzFcD</comment>');
    }
    run('cd {{release_path}}/source/app && {{bin/composer}} {{composer_options}}');
});

set('bin/yarn', function () {
    return run('which yarn');
});

desc('Install Yarn packages');
task('yarn:install', function () {
    if (has('previous_release')) {
        if (test('[ -d {{previous_release}}/source/app/node_modules ]')) {
            run('cp -R {{previous_release}}/source/app/node_modules {{release_path}}/source/app');
        }
    }
    run("cd {{release_path}}/source/app && {{bin/yarn}}");
});

task('yarn:encore', function(){
    run("cd {{release_path}}/source/app/public && yarn encore production");
});

host('ubuntu.loc')
    ->set('deploy_path', '/var/www/{{application}}');
    
// Tasks

task('build', function () {
    run('cd {{release_path}} && build');
});
/**
 * Main task
 */
task('deploy', [
    'deploy:info',
    'deploy:prepare',
    'deploy:lock',
    'deploy:release',
    'deploy:update_code',
    'deploy:shared',
    'deploy:vendors',
    'deploy:clear_paths',
    'yarn:install',
    'yarn:encore',
    'deploy:symlink',
    'deploy:unlock',
    'cleanup',
    'success'
])->desc('Deploy your project');

// Display success message on completion
after('deploy', 'success');
// [Optional] if deploy fails automatically unlock.
after('deploy:failed', 'deploy:unlock');

// Migrate database before symlink new release.
before('deploy:symlink', 'database:migrate');

