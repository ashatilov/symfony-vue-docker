up: docker-up
down: docker-down
restart: docker-down docker-up
init: docker-down-clear clear docker-pull docker-build docker-up app-init

docker-up:
	docker-compose up -d

docker-down:
	docker-compose down --remove-orphans

docker-down-clear:
	docker-compose down -v --remove-orphans

docker-pull:
	docker-compose pull

docker-build:
	docker-compose build

app-init: composer-install assets-install oauth-keys wait-db migrations ready

assets-install:
	docker-compose run --rm node yarn install
	docker-compose run --rm node npm rebuild node-sass

oauth-keys:
	docker-compose run --rm php-cli mkdir -p var/oauth
	docker-compose run --rm php-cli openssl genrsa -out var/oauth/private.key 2048
	docker-compose run --rm php-cli openssl rsa -in var/oauth/private.key -pubout -out var/oauth/public.key
	docker-compose run --rm php-cli chmod 644 var/oauth/private.key var/oauth/public.key

clear:
	docker run --rm -v ${PWD}/source/app:/app --workdir=/app alpine rm -f .ready

composer-install:
	docker-compose run --rm php-cli composer install

wait-db:
	docker-compose run --rm php-cli scripts/wait-for-it.sh mysql:3306 -- echo "db is up"

migrations:
	docker-compose run --rm php-cli php bin/console doctrine:migrations:migrate --no-interaction

fixtures:
	docker-compose run --rm php-cli php bin/console doctrine:fixtures:load --no-interaction

ready:
	docker run --rm -v ${PWD}/source/app:/app --workdir=/app alpine touch .ready

run-dev-server:
	docker-compose run -p 8080:8080 node yarn dev-server

deploy:
	dep deploy